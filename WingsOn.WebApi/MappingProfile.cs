﻿using AutoMapper;
using WingsOn.Core.Models;
using WingsOn.WebApi.ViewModels;

namespace WingsOn.WebApi
{
    /// <summary>
    /// The mapping profile for presentation layer models.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes an instance of <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<PersonViewModel, PersonModel>()
                .ReverseMap();
            CreateMap<BookingViewModel, BookingModel>()
                .ReverseMap();
            CreateMap<PersonPropertiesViewModel, PersonPropertiesModel>()
                .ReverseMap();
        }
    }
}
