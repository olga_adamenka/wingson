﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.WebApi.ViewModels;

namespace WingsOn.WebApi.Controllers
{
    /// <summary>
    /// Processes requests to Booking resource.
    /// </summary>
    [Route("api/bookings")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingService _bookingService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of <see cref="BookingsController"/> class.
        /// </summary>
        /// <param name="bookingService">The booking service.</param>
        /// <param name="mapper">The mapper.</param>
        public BookingsController(IBookingService bookingService, IMapper mapper)
        {
            _bookingService = bookingService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the booking by Id.
        /// The method is not implemented because it's required only for reference.
        /// </summary>
        /// <param name="id">The booking Id.</param>
        /// <returns>The booking.</returns>
        /// <example>GET api/bookings/1</example>
        /// <response code="204">No content.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult GetById(int id)
        {
            // The method is not implemented because it's required only for reference.
            return NoContent();
        }

        /// <summary>
        /// Creates the booking.
        /// </summary>
        /// <param name="booking">The booking to create.</param>
        /// <returns>Location of the created booking.</returns>
        /// <example>POST api/bookings</example>
        /// <response code="201">Returns the newly created booking location.</response>
        /// <response code="400">Request is not valid.</response>
        /// <response code="404">One of related entities is not found.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PostAsync([FromBody]BookingViewModel booking)
        {
            var bookingModel = _mapper.Map<BookingModel>(booking);

            var updatedModel = await _bookingService.CreateBookingAsync(bookingModel);

            return CreatedAtAction(nameof(GetById), new {id = updatedModel.Id}, updatedModel);
        }
    }
}
