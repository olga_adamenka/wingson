﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using WingsOn.Core.Interfaces;
using WingsOn.WebApi.ViewModels;

namespace WingsOn.WebApi.Controllers
{
    /// <summary>
    /// Processes requests to Flight resource.
    /// </summary>
    [Route("api/flights")]
    [ApiController]
    public class FlightsController : ControllerBase
    {
        private readonly IPersonService _personService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of <see cref="BookingsController"/> class.
        /// </summary>
        /// <param name="personService">The person service.</param>
        /// <param name="mapper">The mapper.</param>
        public FlightsController(IPersonService personService, IMapper mapper)
        {
            _personService = personService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets passengers of the flight by flightNumber.
        /// </summary>
        /// <param name="flightNumber">The flight number.</param>
        /// <returns>The list of passengers of the flight.</returns>
        /// <example>GET api/flights/PZ696/passengers</example>
        /// <response code="400">Request is not valid.</response>
        /// <response code="200">List of passengers of the flight.</response>
        [HttpGet("{flightNumber:regex(^[[A-Z]]{{2}}\\d{{3}}$)}/passengers")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<PersonViewModel>> GetPassengersByFlightNumberAsync([BindRequired] string flightNumber)
        {
            var models = await _personService.GetFilteredPassengersAsync(null, flightNumber);
            var viewModels = _mapper.Map<IEnumerable<PersonViewModel>>(models);

            return viewModels;
        }
    }
}
