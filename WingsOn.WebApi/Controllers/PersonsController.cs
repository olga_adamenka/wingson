﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.WebApi.ViewModels;

namespace WingsOn.WebApi.Controllers
{
    /// <summary>
    /// Processes requests to Person resource.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly IPersonService _personService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of <see cref="PersonsController"/> class.
        /// </summary>
        /// <param name="personService">The person service.</param>
        /// <param name="mapper">The mapper.</param>
        public PersonsController(IPersonService personService, IMapper mapper)
        {
            _personService = personService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets person by Id.
        /// </summary>
        /// <param name="id">The person Id.</param>
        /// <returns>The person.</returns>
        /// <example>GET api/persons/5</example>
        /// <response code="404">The person is not found.</response>
        /// <response code="200">The person found by Id.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<PersonViewModel> GetAsync(int id)
        {
            var model = await _personService.GetPersonByIdAsync(id);
            var viewModel = _mapper.Map<PersonViewModel>(model);
            return viewModel;
        }

        /// <summary>
        /// Gets passengers filtered by gender.
        /// </summary>
        /// <param name="gender">The gender filter. Possible values: "male", "female". Null means no filter by gender.</param>
        /// <returns>The list of filtered passengers.</returns>
        /// <example>GET api/persons?gender=male</example>
        /// <response code="400">Request is not valid.</response>
        /// <response code="200">List of filtered persons.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<PersonViewModel>> GetAllAsync(string gender)
        {
            var models = await _personService.GetFilteredPassengersAsync(gender, null);
            var viewModels = _mapper.Map<IEnumerable<PersonViewModel>>(models);

            return viewModels;
        }

        /// <summary>
        /// Updates person properties.
        /// </summary>
        /// <param name="id">The person Id.</param>
        /// <param name="properties">The updated properties.</param>
        /// <returns>No content.</returns>
        /// <example>PATCH api/persons/5</example>
        /// <response code="400">Request is not valid.</response>
        /// <response code="204">Patch is successful.</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchAsync(int id, [FromBody]PersonPropertiesViewModel properties)
        {
            var propertiesModel = _mapper.Map<PersonPropertiesModel>(properties);
            await _personService.UpdatePersonPropertiesAsync(id, propertiesModel);

            return NoContent();
        }
    }
}
