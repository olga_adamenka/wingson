﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WingsOn.WebApi.ViewModels
{
    /// <summary>
    /// The booking view model.
    /// </summary>
    public class BookingViewModel
    {
        public int Id { get; set; }

        [RegularExpression(@"^[A-Z]{2}-\d{6}$", ErrorMessage = "Booking number should have the following format: AA-000000")]
        public string Number { get; set; }

        [Required(ErrorMessage = "FlightId is required")]
        public int FlightId { get; set; }

        [Required(ErrorMessage = "CustomerId is required")]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "PassengerIds is required")]
        public IEnumerable<int> PassengerIds { get; set; }

        [Required(ErrorMessage = "DateBooking is required")]
        public DateTime DateBooking { get; set; }

    }
}
