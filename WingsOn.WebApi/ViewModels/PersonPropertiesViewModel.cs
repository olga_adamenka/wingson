﻿using System.ComponentModel.DataAnnotations;

namespace WingsOn.WebApi.ViewModels
{
    /// <summary>
    /// The person properties view model.
    /// </summary>
    public class PersonPropertiesViewModel
    {
        [MinLength(5, ErrorMessage = "Address is required and shouldn't be less than 5 characters.")]
        public string Address { get; set; }
    }
}
