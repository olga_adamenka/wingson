﻿using System;
using System.ComponentModel.DataAnnotations;
using WingsOn.Domain;

namespace WingsOn.WebApi.ViewModels
{
    /// <summary>
    /// The person view model.
    /// </summary>
    public class PersonViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "DateBirth is required.")]
        [DataType(DataType.Date, ErrorMessage = "DateBirth should be a valid date.")]
        public DateTime DateBirth { get; set; }

        [Required(ErrorMessage = "Gender is required.")]
        [EnumDataType(typeof(GenderType), ErrorMessage = "Gender should be either Male or Female.")]
        public GenderType Gender { get; set; }

        [MinLength(5, ErrorMessage = "Address is required and shouldn't be less than 5 characters.")]
        public string Address { get; set; }

        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email should be a valid email address.")]
        public string Email { get; set; }
    }
}
