﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WingsOn.Core.Exceptions;

namespace WingsOn.WebApi
{
    /// <summary>
    /// The middleware to handle application exceptions and convert them into corresponding status codes.
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<Exception> logger)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, logger);
            }
        }

        /// <summary>
        /// Processes application exceptions and converts them into HTTP status codes.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="ex">The exception to process.</param>
        /// <param name="logger">The logger.</param>
        private static Task HandleExceptionAsync(HttpContext context, Exception ex, ILogger<Exception> logger)
        {
            var code = HttpStatusCode.InternalServerError; 

            // Process application logic exception.
            if (ex is EntityNotFoundException)
            {
                code = HttpStatusCode.NotFound;
            }
            else if (ex is ValidationException)
            {
                code = HttpStatusCode.BadRequest;
            }

            if (code == HttpStatusCode.InternalServerError)
            {
                logger.LogError(ex, "Processing request exception");
            }

            var result = JsonConvert.SerializeObject(new { error = ex.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
