using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using DeepEqual.Syntax;
using Moq;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Services.Tests
{
    public class BookingServiceTests
    {
        private readonly Mock<IRepository<Booking>> _bookingRepositoryMock;
        private readonly Mock<IRepository<Person>> _personRepositoryMock;
        private readonly Mock<IRepository<Flight>> _flightRepositoryMock;
        private readonly IMapper _mapper;

        public BookingServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _bookingRepositoryMock = new Mock<IRepository<Booking>>(MockBehavior.Strict);
            _personRepositoryMock = new Mock<IRepository<Person>>(MockBehavior.Strict);
            _flightRepositoryMock = new Mock<IRepository<Flight>>(MockBehavior.Strict);
        }

        [Fact]
        public async Task CreateBookingAsync_CorrectBooking_SavesToUnderlyingRepository()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var flight = new Flight {Id = booking.FlightId};
            var customer = new Person {Id = booking.CustomerId};
            var passengers = new[] {new Person {Id = 1}, customer};
            booking.PassengerIds = passengers.Select(x => x.Id);

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns(flight);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns(customer);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(passengers);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var result = await service.CreateBookingAsync(booking);

            // Assert
            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
            result.WithDeepEqual(booking)
                .IgnoreSourceProperty(x => x.Id)
                .Assert();
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Once());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Once());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Once());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Once());
        }

        [Fact]
        public async Task CreateBookingAsync_FlightNotFound_EntityNotFoundException()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var customer = new Person { Id = booking.CustomerId };
            var passengers = new[] { new Person { Id = 1 }, customer };
            booking.PassengerIds = passengers.Select(x => x.Id);

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns((Flight)null);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns(customer);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(passengers);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => service.CreateBookingAsync(booking));

            // Assert
            Assert.Contains("flight", exception.Message, StringComparison.OrdinalIgnoreCase);
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Never());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Never());
        }

        [Fact]
        public async Task CreateBookingAsync_CustomerNotFound_EntityNotFoundException()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var flight = new Flight { Id = booking.FlightId };
            var passengers = new[] { new Person { Id = 1 } };
            booking.PassengerIds = passengers.Select(x => x.Id);

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns(flight);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns((Person)null);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(passengers);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => service.CreateBookingAsync(booking));

            // Assert
            Assert.Contains("customer", exception.Message, StringComparison.OrdinalIgnoreCase);
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Once());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Never());
        }

        [Fact]
        public async Task CreateBookingAsync_EmptyPassengers_ValidationException()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var flight = new Flight { Id = booking.FlightId };
            var customer = new Person { Id = booking.CustomerId };
            var passengers = new Person[0];
            booking.PassengerIds = passengers.Select(x => x.Id);

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns(flight);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns(customer);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(passengers);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(() => service.CreateBookingAsync(booking));

            // Assert
            Assert.Contains("passenger", exception.Message, StringComparison.OrdinalIgnoreCase);
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Once());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Never());
        }

        [Fact]
        public async Task CreateBookingAsync_NullPassengers_ValidationException()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var flight = new Flight { Id = booking.FlightId };
            var customer = new Person { Id = booking.CustomerId };
            var passengers = new Person[0];
            booking.PassengerIds = null;

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns(flight);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns(customer);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(passengers);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(() => service.CreateBookingAsync(booking));

            // Assert
            Assert.Contains("passenger", exception.Message, StringComparison.OrdinalIgnoreCase);
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Once());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Never());
        }

        [Fact]
        public async Task CreateBookingAsync_PassengerNotFound_EntityNotFoundException()
        {
            // Arrange
            var fixture = new Fixture();
            var booking = fixture.Create<BookingModel>();
            booking.Id = 0;
            var flight = new Flight { Id = booking.FlightId };
            var customer = new Person { Id = booking.CustomerId };
            var passengers = new[] { new Person { Id = 1 }, customer };
            booking.PassengerIds = passengers.Select(x => x.Id);

            _flightRepositoryMock.Setup(x => x.Get(booking.FlightId)).Returns(flight);
            _personRepositoryMock.Setup(x => x.Get(booking.CustomerId)).Returns(customer);
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(new[] {customer});
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(Array.Empty<Booking>());
            _bookingRepositoryMock.Setup(x => x.Save(It.IsAny<Booking>()));

            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => service.CreateBookingAsync(booking));

            // Assert
            Assert.Contains("passenger", exception.Message, StringComparison.OrdinalIgnoreCase);
            _flightRepositoryMock.Verify(x => x.Get(booking.FlightId), Times.Once());
            _personRepositoryMock.Verify(x => x.Get(booking.CustomerId), Times.Once());
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Once());
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never());
            _bookingRepositoryMock.Verify(x => x.Save(It.IsAny<Booking>()), Times.Never());
        }

        private BookingService CreateService()
        {
            var service = new BookingService(
                _bookingRepositoryMock.Object,
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object, 
                _mapper);
            return service;
        }
    }
}
