using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using DeepEqual.Syntax;
using Moq;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.Services.Tests
{
    public class PersonServiceTests
    {
        private readonly Mock<IRepository<Person>> _personRepositoryMock;
        private readonly Mock<IRepository<Booking>> _bookingRepositoryMock;
        private readonly IMapper _mapper;
        private const string FlightNumber = "PZ100";
        private Person[] _personData;
        private Booking[] _bookingData;

        public PersonServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _personRepositoryMock = new Mock<IRepository<Person>>(MockBehavior.Strict);
            _bookingRepositoryMock = new Mock<IRepository<Booking>>(MockBehavior.Strict);

            SetupMockData();
        }

        [Fact]
        public async Task GetPersonByIdAsync_ExistingPerson_ReturnsCorrectPerson()
        {
            // Arrange
            var fixture = new Fixture();
            var person = fixture.Create<Person>();
            _personRepositoryMock
                .Setup(x => x.Get(person.Id))
                .Returns(person);
            var service = CreateService();

            // Act
            var result = await service.GetPersonByIdAsync(person.Id);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(person);
            _personRepositoryMock.Verify(x => x.Get(person.Id), Times.Once());
        }

        [Fact]
        public async Task GetPersonByIdAsync_NoPerson_EntityNotFoundException()
        {
            // Arrange
            var personId = 100;
            _personRepositoryMock
                .Setup(x => x.Get(personId))
                .Returns((Person)null);
            var service = CreateService();

            // Act
            await Assert.ThrowsAsync<EntityNotFoundException>(() => service.GetPersonByIdAsync(personId));

            // Assert
            _personRepositoryMock.Verify(x => x.Get(personId), Times.Once());
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_CorrectParameters_ReturnsFilteredData()
        {
            // Arrange
            var gender = "male";
            var flightNumber = FlightNumber;
            var expectedData = new[] {_personData[0]};
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var result = await service.GetFilteredPassengersAsync(gender, flightNumber);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(expectedData);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_OnlyGenderParameter_ReturnsFilteredData()
        {
            // Arrange
            var gender = "male";
            var expectedData = new[] { _personData[0], _personData[2] };
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var result = await service.GetFilteredPassengersAsync(gender, null);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(expectedData);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_OnlyFlightNumberParameter_ReturnsFilteredData()
        {
            // Arrange
            var flightNumber = FlightNumber;
            var expectedData = new[] { _personData[0], _personData[1] };
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var result = await service.GetFilteredPassengersAsync(null, flightNumber);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(expectedData);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_NoParameters_ReturnsFilteredData()
        {
            // Arrange
            var expectedData = new[] { _personData[0], _personData[1], _personData[2] };
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var result = await service.GetFilteredPassengersAsync(null, null);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(expectedData);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_IncorrectGenderParameters_ValidationException()
        {
            // Arrange
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => service.GetFilteredPassengersAsync("none", null));

            // Assert
            Assert.Contains("gender", exception.Message, StringComparison.OrdinalIgnoreCase);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never);
        }

        [Fact]
        public async Task GetFilteredPassengersAsync_IncorrectFlightNumberParameters_ValidationException()
        {
            // Arrange
            _personRepositoryMock.Setup(x => x.GetAll()).Returns(_personData);
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(_bookingData);
            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => service.GetFilteredPassengersAsync(null, "PZZ100"));

            // Assert
            Assert.Contains("flightNumber", exception.Message, StringComparison.OrdinalIgnoreCase);
            _personRepositoryMock.Verify(x => x.GetAll(), Times.Never);
            _bookingRepositoryMock.Verify(x => x.GetAll(), Times.Never);
        }

        [Fact]
        public async Task UpdatePersonPropertiesAsync_CorrectParameters_SavesToUnderlyingRepository()
        {
            // Arrange
            var personId = 100;
            var newAddress = "new address";
            var personProperties = new PersonPropertiesModel {Address = newAddress};
            _personRepositoryMock.Setup(x => x.Get(personId)).Returns(new Person {Id = personId});
            _personRepositoryMock.Setup(x => x.Save(It.IsAny<Person>()));
            var service = CreateService();

            // Act
            await service.UpdatePersonPropertiesAsync(personId, personProperties);

            // Assert
            _personRepositoryMock.Verify(x => x.Save(It.Is<Person>(p => p.Address == newAddress)), Times.Once);
        }

        [Fact]
        public async Task UpdatePersonPropertiesAsync_EmptyPropertiesParameter_ValidationException()
        {
            // Arrange
            var personId = 100;
            _personRepositoryMock.Setup(x => x.Get(personId)).Returns(new Person { Id = personId });
            _personRepositoryMock.Setup(x => x.Save(It.IsAny<Person>()));
            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(() => service.UpdatePersonPropertiesAsync(personId, null));

            // Assert
            Assert.Contains("properties", exception.Message, StringComparison.OrdinalIgnoreCase);
            _personRepositoryMock.Verify(x => x.Save(It.IsAny<Person>()), Times.Never);
        }

        [Fact]
        public async Task UpdatePersonPropertiesAsync_NonExistingPersonParameter_EntityNotFoundException()
        {
            // Arrange
            var personId = 111;
            var newAddress = "new address";
            var personProperties = new PersonPropertiesModel { Address = newAddress };
            _personRepositoryMock.Setup(x => x.Get(personId)).Returns((Person)null);
            _personRepositoryMock.Setup(x => x.Save(It.IsAny<Person>()));
            var service = CreateService();

            // Act
            await Assert.ThrowsAsync<EntityNotFoundException>(
                () => service.UpdatePersonPropertiesAsync(personId, personProperties));

            // Assert
            _personRepositoryMock.Verify(x => x.Save(It.IsAny<Person>()), Times.Never);
        }

        [Fact]
        public async Task UpdatePersonPropertiesAsync_EmptyPropertiesAddressParameter_ValidationException()
        {
            // Arrange
            var personId = 100;
            var personProperties = new PersonPropertiesModel { Address = string.Empty };
            _personRepositoryMock.Setup(x => x.Get(personId)).Returns(new Person { Id = personId });
            _personRepositoryMock.Setup(x => x.Save(It.IsAny<Person>()));
            var service = CreateService();

            // Act
            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => service.UpdatePersonPropertiesAsync(personId, personProperties));

            // Assert
            Assert.Contains("address", exception.Message, StringComparison.OrdinalIgnoreCase);
            _personRepositoryMock.Verify(x => x.Save(It.IsAny<Person>()), Times.Never);
        }

        private void SetupMockData()
        {
            var fixture = new Fixture();

            _personData = fixture.CreateMany<Person>(4).ToArray();
            // 0 and 1 - for flight "PZ100"
            _personData[0].Gender = GenderType.Male;
            _personData[1].Gender = GenderType.Female;
            // 2 - for flight "not PZ100"
            _personData[2].Gender = GenderType.Male;
            // 3 - not a passenger
            _personData[3].Gender = GenderType.Male;

            _bookingData = fixture.CreateMany<Booking>(3).ToArray();
            _bookingData[0].Flight.Number = FlightNumber;
            _bookingData[0].Passengers = new[] {_personData[0], _personData[1]};
            _bookingData[1].Flight.Number = "not " + FlightNumber;
            _bookingData[1].Passengers = new[] { _personData[2] };
            _bookingData[2].Flight.Number = FlightNumber;
            _bookingData[2].Passengers = new[] { _personData[0] };
        }

        private PersonService CreateService()
        {
            return new PersonService(_personRepositoryMock.Object, _bookingRepositoryMock.Object, _mapper);
        }
    }
}
