﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Services
{
    /// <summary>
    /// The booking service.
    /// </summary>
    public class BookingService : IBookingService
    {
        private readonly IRepository<Booking> _bookingRepository;
        private readonly IRepository<Person> _personRepository;
        private readonly IRepository<Flight> _flightRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Just to generate ids and avoid potential race conditions.
        /// </summary>
        private static readonly object NewIdLock = new object();

        /// <summary>
        /// Initializes a new instance of <see cref="BookingService"/> class.
        /// </summary>
        /// <param name="bookingRepository">The booking repository.</param>
        /// <param name="personRepository">The person repository.</param>
        /// <param name="flightRepository">The flight repository.</param>
        /// <param name="mapper">The mapper.</param>
        public BookingService(
            IRepository<Booking> bookingRepository, 
            IRepository<Person> personRepository, 
            IRepository<Flight> flightRepository,
            IMapper mapper)
        {
            _bookingRepository = bookingRepository;
            _personRepository = personRepository;
            _flightRepository = flightRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates the booking in the repository.
        /// </summary>
        /// <param name="model">The booking to create.</param>
        /// <returns>The created booking.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when flight, customer or passengers are not found in repositories.</exception>
        /// <exception cref="ValidationException">Thrown when there are no passengers in the booking.</exception>
        public async Task<BookingModel> CreateBookingAsync(BookingModel model)
        {
            var flight = await GetFlightAsync(model);
            var customer = await GetCustomerAsync(model);
            var passengers = await GetPassengersAsync(model);

            // We generate the new Id just because the given repository doesn't. When working with real databases/services this would be in DB
            var newId = GenerateNewId();

            var booking = _mapper.Map<Booking>(model);
            booking.Id = newId;
            booking.Flight = flight;
            booking.Customer = customer;
            booking.Passengers = passengers;

            _bookingRepository.Save(booking);

            var updatedModel = _mapper.Map<BookingModel>(booking);

            return updatedModel;
        }

        /// <summary>
        /// Generates new id for the booking because the repository doesn't create it.
        /// </summary>
        /// <returns>The unique booking id.</returns>
        private int GenerateNewId()
        {
            lock (NewIdLock)
            {
                var bookings = _bookingRepository.GetAll().ToArray();
                int newId = bookings.Any() ? bookings.Max(x => x.Id) + 1 : 1;
                return newId;
            }
        }

        /// <summary>
        /// Gets passengers for the booking from the repository.
        /// </summary>
        /// <param name="model">The booking model.</param>
        /// <returns>The passengers.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when passengers are not found in the repository.</exception>
        /// <exception cref="ValidationException">Thrown when there are no passengers in the booking.</exception>
        private async Task<List<Person>> GetPassengersAsync(BookingModel model)
        {
            if (model.PassengerIds == null || !model.PassengerIds.Any())
            {
                throw new ValidationException("there should be at least one passenger in the booking.");
            }

            var allPassengers = await Task.FromResult(_personRepository.GetAll());
            var passengers = allPassengers
                .Where(x => model.PassengerIds.Contains(x.Id))
                .ToList();
            if (passengers.Count != model.PassengerIds.Count())
            {
                throw new EntityNotFoundException($"at least one of {nameof(passengers)} is not found.");
            }

            return passengers;
        }

        /// <summary>
        /// Gets the customer for the booking from the repository.
        /// </summary>
        /// <param name="model">The booking model.</param>
        /// <returns>The customer.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when customer is not found in the repository.</exception>
        private async Task<Person> GetCustomerAsync(BookingModel model)
        {
            var customer = await Task.FromResult(_personRepository.Get(model.CustomerId));
            if (customer == null)
            {
                throw new EntityNotFoundException($"{nameof(customer)} is not found.");
            }

            return customer;
        }

        /// <summary>
        /// Gets the flight for the booking from the repository.
        /// </summary>
        /// <param name="model">The booking model.</param>
        /// <returns>The flight.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when flight is not found in the repository.</exception>
        private async Task<Flight> GetFlightAsync(BookingModel model)
        {
            var flight = await Task.FromResult(_flightRepository.Get(model.FlightId));
            if (flight == null)
            {
                throw new EntityNotFoundException($"{nameof(flight)} is not found.");
            }

            return flight;
        }
    }
}
