﻿using System.Linq;
using AutoMapper;
using WingsOn.Core.Models;
using WingsOn.Domain;

namespace WingsOn.Services
{
    /// <summary>
    /// The mapping profile for business layer models.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes an instance of <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Person, PersonModel>()
                .ReverseMap();
            CreateMap<Booking, BookingModel>()
                .ForMember(x => x.CustomerId, opt => opt.MapFrom(x => x.Customer.Id))
                .ForMember(x => x.FlightId, opt => opt.MapFrom(x => x.Flight.Id))
                .ForMember(x => x.PassengerIds, opt => opt.MapFrom(x => x.Passengers.Select(y => y.Id)))
                .ReverseMap();
        }
    }
}
