﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Services
{
    /// <summary>
    /// The person service.
    /// </summary>
    public class PersonService : IPersonService
    {
        /// <summary>
        /// The regex to validate flight number.
        /// </summary>
        private static readonly Regex FlightNumberRegex = new Regex("^[A-Z]{2}[0-9]{1,4}$");

        private readonly IRepository<Person> _personRepository;
        private readonly IRepository<Booking> _bookingRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of <see cref="PersonService"/> class.
        /// </summary>
        /// <param name="personRepository">The person repository.</param>
        /// <param name="bookingRepository">The booking repository.</param>
        /// <param name="mapper">The mapper.</param>
        public PersonService(IRepository<Person> personRepository, IRepository<Booking> bookingRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _bookingRepository = bookingRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the person by Id.
        /// </summary>
        /// <param name="id">The person Id.</param>
        /// <returns>The person model.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when a person is not found in the repository.</exception>
        public async Task<PersonModel> GetPersonByIdAsync(int id)
        {
            var person = await Task.FromResult(_personRepository.Get(id));
            if (person == null)
            {
                throw new EntityNotFoundException();
            }

            var personModel = _mapper.Map<PersonModel>(person);

            return personModel;
        }

        /// <summary>
        /// Get passengers filtered by gender and flight number.
        /// </summary>
        /// <param name="gender">The gender to filter. Possible values: "male", "female". Null means no filter by gender.</param>
        /// <param name="flightNumber">The flight number to filter. Null means no filter by flight number.</param>
        /// <returns>The filtered passengers.</returns>
        /// <remarks>
        /// The methods returns only those persons who are passengers at least in one flight.
        /// People that have no flights/bookings will not be returned.
        /// </remarks>
        /// <exception cref="ValidationException">Thrown when filter values are not correct.</exception>
        public async Task<IEnumerable<PersonModel>> GetFilteredPassengersAsync(string gender, string flightNumber)
        {
            var parsedGender = ParseGender(gender);
            var parsedFlightNumber = ParseFlightNumber(flightNumber);

            var bookings = await Task.FromResult(_bookingRepository.GetAll());
            if (parsedFlightNumber != null)
            {
                bookings = bookings.Where(x => x.Flight.Number == flightNumber);
            }

            var persons = bookings
                .SelectMany(booking => booking.Passengers)
                .Distinct();

            if (parsedGender != null)
            {
                persons = persons.Where(x => x.Gender == parsedGender);
            }

            var personModels = _mapper.Map<IEnumerable<PersonModel>>(persons);

            return personModels;
        }

        /// <summary>
        /// Updates person properties.
        /// </summary>
        /// <param name="id">The person Id to update.</param>
        /// <param name="properties">The properties to update. Null properties are considered as not modified.</param>
        /// <exception cref="ValidationException">Thrown when properties are not correct.</exception>
        /// <exception cref="EntityNotFoundException">Thrown when a person is not found in the repository.</exception>
        public async Task UpdatePersonPropertiesAsync(int id, PersonPropertiesModel properties)
        {
            if (properties == null)
            {
                throw new ValidationException($"{nameof(properties)} shouldn't be empty.");
            }

            var person = await Task.FromResult(_personRepository.Get(id));
            if (person == null)
            {
                throw new EntityNotFoundException();
            }

            if (properties.Address != null)
            {
                ValidateAddress(properties.Address);
                person.Address = properties.Address;
            }

            _personRepository.Save(person);
        }

        /// <summary>
        /// Validates that person's address is not empty.
        /// </summary>
        /// <param name="address">The address to validate.</param>
        /// <exception cref="ValidationException">Thrown when properties are not correct.</exception>
        private void ValidateAddress(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                throw new ValidationException($"{nameof(address)} shouldn't be empty.");
            }
        }

        /// <summary>
        /// Parses and validates flight number filter.
        /// </summary>
        /// <param name="flightNumber">The flight number to parse.</param>
        /// <returns>The parsed flight number.</returns>
        /// <exception cref="ValidationException">Thrown when flight number is not correct.</exception>
        private string ParseFlightNumber(string flightNumber)
        {
            if (string.IsNullOrEmpty(flightNumber))
            {
                return null;
            }

            if (!FlightNumberRegex.IsMatch(flightNumber))
            {
                throw new ValidationException($"{nameof(flightNumber)} is incorrect.");
            }

            return flightNumber;
        }

        /// <summary>
        /// Parses and validates gender filter.
        /// </summary>
        /// <param name="gender">The gender to parse.</param>
        /// <returns>The parsed gender.</returns>
        /// <exception cref="ValidationException">Thrown when gender is not correct.</exception>
        private static GenderType? ParseGender(string gender)
        {
            if (string.IsNullOrEmpty(gender))
            {
                return null;
            }

            if (!Enum.TryParse<GenderType>(gender, true, out var genderType))
            {
                throw new ValidationException($"{nameof(gender)} is incorrect.");
            }

            return genderType;
        }
    }
}
