using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.WebApi.Controllers;
using WingsOn.WebApi.ViewModels;
using Xunit;

namespace WingsOn.WebApi.Tests.Controllers
{
    public class PersonsControllerTests
    {
        private readonly Mock<IPersonService> _personServiceMock;
        private readonly IMapper _mapper;

        public PersonsControllerTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _personServiceMock = new Mock<IPersonService>(MockBehavior.Strict);
        }

        [Fact]
        public async Task GetAsync_CorrectData_CallsUnderlyingService()
        {
            // Arrange
            var personId = 10;
            var fixture = new Fixture();
            var personModel = fixture.Create<PersonModel>();
            _personServiceMock
                .Setup(x => x.GetPersonByIdAsync(personId))
                .ReturnsAsync(personModel);
            var controller = CreateController();

            // Act
            var result = await controller.GetAsync(personId);

            // Assert

            Assert.NotNull(result);
            result.ShouldDeepEqual(personModel);
            _personServiceMock.Verify(x => x.GetPersonByIdAsync(personId), Times.Once());
        }

        [Fact]
        public async Task GetAllAsync_CorrectData_CallsUnderlyingService()
        {
            // Arrange
            var gender = "male";
            var fixture = new Fixture();
            var personModels = fixture.CreateMany<PersonModel>();
            _personServiceMock
                .Setup(x => x.GetFilteredPassengersAsync(gender, null))
                .ReturnsAsync(personModels);
            var controller = CreateController();

            // Act
            var result = await controller.GetAllAsync(gender);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(personModels);
            _personServiceMock.Verify(x => x.GetFilteredPassengersAsync(gender, null), Times.Once());
        }

        [Fact]
        public async Task PatchAsync_CorrectData_CallsUnderlyingService()
        {
            // Arrange
            var personId = 10;
            var personProperties = new PersonPropertiesViewModel();
            _personServiceMock
                .Setup(x => x.UpdatePersonPropertiesAsync(personId, It.IsAny<PersonPropertiesModel>()))
                .Returns(Task.CompletedTask);
            var controller = CreateController();

            // Act
            var result = await controller.PatchAsync(personId, personProperties);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<NoContentResult>(result);
            _personServiceMock.Verify(x => x.UpdatePersonPropertiesAsync(personId, It.IsAny<PersonPropertiesModel>()), Times.Once());
        }

        private PersonsController CreateController()
        {
            var controller = new PersonsController(_personServiceMock.Object, _mapper);
            return controller;
        }
    }
}
