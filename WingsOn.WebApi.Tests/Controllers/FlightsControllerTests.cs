using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using DeepEqual.Syntax;
using Moq;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.WebApi.Controllers;
using Xunit;

namespace WingsOn.WebApi.Tests.Controllers
{
    public class FlightsControllerTests
    {
        private readonly Mock<IPersonService> _personServiceMock;
        private readonly IMapper _mapper;

        public FlightsControllerTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _personServiceMock = new Mock<IPersonService>(MockBehavior.Strict);
        }

        [Fact]
        public async Task GetPassengersByFlightNumberAsync_CorrectData_CallsUnderlyingService()
        {
            // Arrange
            var flightNumber = "BB111";
            var fixture = new Fixture();
            var personModels = fixture.CreateMany<PersonModel>();
            _personServiceMock
                .Setup(x => x.GetFilteredPassengersAsync(null, flightNumber))
                .ReturnsAsync(personModels);
            var controller = CreateController();

            // Act
            var result = await controller.GetPassengersByFlightNumberAsync(flightNumber);

            // Assert
            Assert.NotNull(result);
            result.ShouldDeepEqual(personModels);
            _personServiceMock.Verify(x => x.GetFilteredPassengersAsync(null, flightNumber), Times.Once());
        }

        private FlightsController CreateController()
        {
            var controller = new FlightsController(_personServiceMock.Object, _mapper);
            return controller;
        }
    }
}
