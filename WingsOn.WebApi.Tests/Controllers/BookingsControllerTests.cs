﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WingsOn.Core.Interfaces;
using WingsOn.Core.Models;
using WingsOn.WebApi.Controllers;
using WingsOn.WebApi.ViewModels;
using Xunit;

namespace WingsOn.WebApi.Tests.Controllers
{
    public class BookingsControllerTests
    {
        private readonly Mock<IBookingService> _bookingServiceMock;
        private readonly IMapper _mapper;

        public BookingsControllerTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _bookingServiceMock = new Mock<IBookingService>(MockBehavior.Strict);
        }

        [Fact]
        public async Task PostAsync_CorrectData_CallsUnderlyingService()
        {
            // Arrange
            int newBookingId = 5;
            var bookingViewModel = new BookingViewModel();

            var bookingModel = new BookingModel
            {
                Id = newBookingId
            };

            _bookingServiceMock
                .Setup(x => x.CreateBookingAsync(It.IsAny<BookingModel>()))
                .ReturnsAsync(bookingModel);

            var controller = new BookingsController(_bookingServiceMock.Object, _mapper);

            // Act
            var result = await controller.PostAsync(bookingViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<CreatedAtActionResult>(result);
            _bookingServiceMock.Verify(x => x.CreateBookingAsync(It.IsAny<BookingModel>()), Times.Once);
        }
    }
}
