﻿using System.Threading.Tasks;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;

namespace WingsOn.Core.Interfaces
{
    /// <summary>
    /// The booking service.
    /// </summary>
    public interface IBookingService
    {
        /// <summary>
        /// Creates the booking in the repository.
        /// </summary>
        /// <param name="model">The booking to create.</param>
        /// <returns>The created booking.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when flight, customer or passengers are not found in repositories.</exception>
        /// <exception cref="ValidationException">Thrown when there are no passengers in the booking.</exception>
        Task<BookingModel> CreateBookingAsync(BookingModel model);
    }
}
