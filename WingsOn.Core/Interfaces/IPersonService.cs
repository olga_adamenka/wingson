﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WingsOn.Core.Exceptions;
using WingsOn.Core.Models;

namespace WingsOn.Core.Interfaces
{
    /// <summary>
    /// The person service.
    /// </summary>
    public interface IPersonService
    {
        /// <summary>
        /// Gets the person by Id.
        /// </summary>
        /// <param name="id">The person Id.</param>
        /// <returns>The person model.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when a person is not found in the repository.</exception>
        Task<PersonModel> GetPersonByIdAsync(int id);

        /// <summary>
        /// Get passengers filtered by gender and flight number.
        /// </summary>
        /// <param name="gender">The gender to filter. Possible values: "male", "female". Null means no filter by gender.</param>
        /// <param name="flightNumber">The flight number to filter. Null means no filter by flight number.</param>
        /// <returns>The filtered passengers.</returns>
        /// <remarks>
        /// The methods returns only those persons who are passengers at least in one flight.
        /// People that have no flights/bookings will not be returned.
        /// </remarks>
        /// <exception cref="ValidationException">Thrown when filter values are not correct.</exception>
        Task<IEnumerable<PersonModel>> GetFilteredPassengersAsync(string gender, string flightNumber);

        /// <summary>
        /// Updates person properties.
        /// </summary>
        /// <param name="id">The person Id to update.</param>
        /// <param name="properties">The properties to update. Null properties are considered as not modified.</param>
        /// <exception cref="ValidationException">Thrown when properties are not correct.</exception>
        /// <exception cref="EntityNotFoundException">Thrown when a person is not found in the repository.</exception>
        Task UpdatePersonPropertiesAsync(int id, PersonPropertiesModel properties);
    }
}
