﻿using System;

namespace WingsOn.Core.Exceptions
{
    /// <summary>
    /// The exception to throw when validation of input data is failed.
    /// </summary>
    public class ValidationException : Exception
    {
        public ValidationException()
        {
        }

        public ValidationException(string message)
            : base(message)
        {
        }

        public ValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
