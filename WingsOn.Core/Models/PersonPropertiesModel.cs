﻿namespace WingsOn.Core.Models
{
    /// <summary>
    /// The person properties model.
    /// </summary>
    public class PersonPropertiesModel
    {
        public string Address { get; set; }
    }
}
