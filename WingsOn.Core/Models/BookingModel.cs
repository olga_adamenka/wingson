﻿using System;
using System.Collections.Generic;

namespace WingsOn.Core.Models
{
    /// <summary>
    /// The booking model.
    /// </summary>
    public class BookingModel
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public int FlightId { get; set; }

        public int CustomerId { get; set; }

        public IEnumerable<int> PassengerIds { get; set; }

        public DateTime DateBooking { get; set; }

    }
}
